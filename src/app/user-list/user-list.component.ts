import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { User } from '../models/user.type';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['givenName', 'familyName', 'dateOfBirth', 'itemNum', 'actions'];
  users: Observable<User[]>;

  constructor(private readonly dialog: MatDialog, private readonly userService: UserService) {}

  ngOnInit() {
    this.users = this.userService.getAll();
  }

  openAddUserModal() {
    const dialogRef = this.dialog.open(EditUserComponent);
  }

  editUserModal(user: User) {
    const dialogRef = this.dialog.open(EditUserComponent);
    const componentInstance = dialogRef.componentInstance;

    // pass down all input parameters
    componentInstance.user = user;
  }

  deleteUser(user: User) {
    this.userService.delete(user);
  }
}
