import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from './models/user.type';

// the service uses references to track entites. In real app primary keys have to be used.
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private users = new BehaviorSubject<User[]>(this.createDummyState());

  constructor() {}

  public getAll(): Observable<User[]> {
    return this.users.asObservable();
  }

  public add(user: User) {
    this.users.next([...this.users.getValue(), user]);
  }

  public delete(user: User) {
    const newUserList = this.users.getValue().filter(u => u !== user);
    this.users.next(newUserList);
  }

  public update(user: User) {
    const userList = this.users.getValue();
    const index = userList.indexOf(user);

    userList[index] = user;

    this.users.next([...userList]);
  }

  private createInitialState(): User[] {
    return [];
  }

  private createDummyState(): User[] {
    return [
      {
        givenName: 'Ali',
        familyName: 'Delshad',
        dateOfBirth: new Date(1983, 0, 11),
        itemNum: 1
      },
      {
        givenName: 'Hamid',
        familyName: 'Sadeghi',
        dateOfBirth: new Date(1984, 1, 12),
        itemNum: 1
      },
      {
        givenName: 'Amir',
        familyName: 'Olfat',
        dateOfBirth: new Date(1985, 2, 13),
        itemNum: 1
      },
      {
        givenName: 'Keyvan',
        familyName: 'Nasr',
        dateOfBirth: new Date(1986, 3, 14),
        itemNum: 1
      }
    ];
  }
}
