import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { User } from '../models/user.type';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { UserService } from '../user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditUserComponent implements OnInit {
  @Input() user: User;
  form: FormGroup;
  editMode = false;

  constructor(
    private readonly fb: FormBuilder,
    private readonly dialogRef: MatDialogRef<EditUserComponent>,
    private readonly userService: UserService
  ) {}

  ngOnInit() {
    this.form = this.fb.group(
      {
        givenName: ['', [Validators.required]],
        familyName: ['', [Validators.required]],
        dateOfBirth: ['', [Validators.required]],
        itemNum: ['', [Validators.required]]
      },
      { updateOn: 'blur' }
    );

    if (this.user) {
      this.editMode = true;
      this.form.setValue(this.user);
    }
  }

  saveAndClose() {
    if (this.form.invalid) {
      return;
    }

    if (this.editMode) {
      Object.assign(this.user, this.form.value);
      this.userService.update(this.user);
    } else {
      this.userService.add(this.form.value);
    }

    this.dialogRef.close();
  }

  cancel() {
    this.dialogRef.close();
  }
}
