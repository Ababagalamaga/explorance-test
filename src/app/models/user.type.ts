export interface User {
  givenName: string;
  familyName: string;
  dateOfBirth: Date;
  itemNum: number;
}
